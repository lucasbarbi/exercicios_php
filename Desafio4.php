<?php

// Verifica se o metodo e POST
if($_SERVER['REQUEST_METHOD']=='POST') {
    $name = '';
    if (isset($_POST['nome'])) {
        $nome = $_POST['nome'];
    }
    $sobrenome = '';
    if (isset($_POST['sobrenome'])) {
        $sobrenome = $_POST['sobrenome'];
    }
    $email = '';
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    $telefone = '';
    if (isset($_POST['telefone'])) {
        $telefone = $_POST['telefone'];
    }
    $usuario = '';
    if (isset($_POST['usuario'])) {
        $usuario = $_POST['usuario'];

    }
    $senha = '';
    if (isset($_POST['senha'])) {
        $senha = $_POST['senha'];
    }

    // Abre o arquivo registro.txt e pega seu conteudo
    fopen('registros.txt','a');
    $data = file_get_contents("registros.txt");
    // Verifica se o e-email já esta cadastrado
    if(strstr($data, $email)) {
        echo 'Email ja cadastrado';
    } else {
        // Verifica se o usuario jé esta cadastrado
        if (strstr($data, $usuario)) {
            echo 'Usuario ja cadastrado';

        } else {
            // Verifica se o email é valido
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                // Verifica se o numero de telefoe é valido e com 11 digitos
                if (preg_match("/^[0-9]{2}[0-9]{5}[0-9]{4}$/", $telefone)) {

                    $novocad = [
                        'Nome' => $nome,
                        'Sobrenome' => $sobrenome,
                        'Email' => $email,
                        'Telefone' => $telefone,
                        'Usuario' => $usuario,
                        'Senha' => $senha = (crypt($senha))
                    ];
                    // Encriptografando a senha
                    $cod = md5($senha);
                    // Grava os dados informados no arquivo registros.txt
                    $json_data = json_encode($novocad, JSON_PRETTY_PRINT);
                    file_put_contents("registros.txt", $json_data, FILE_APPEND);
                    header("Location: index.php");die;
                } else {
                    echo 'Telefone nao e valido! Favor usar apenas numeros e com 11 digitos';
                }
            } else {
                // Verifica se o telefone tambem esta errado depois de verificar que o email esta invalido
                if (preg_match("/^[0-9]{2}[0-9]{5}[0-9]{4}$/", $telefone)) {
                    echo 'E-mail invalido';
                } else {
                    echo 'E-mail e telefone invalidos!';
                }
            }

        }

    }

}

?>

<div class="container">

    <h1>Cadastro</h1>
    <form action="desafio4.php" method="POST">
        <div class="form-group">
            <label for="name" class="control-label">Nome</label>
            <input type="text" name="nome" class="form-control" required/>
        </div>
        <div class="form-group">
            <label for="last-name" class="control-label">Sobrenome</label>
            <input type="text" name="sobrenome" class="form-control" required/>
        </div>
        <div class="form-group">
            <label for="idemail" class="control-label">E-mail</label>
            <input type="text" name="email" class="form-control" required/>
        </div>
        <div class="form-group">
            <label for="tel" class="control-label">Telefone</label>
            <input type="text" name="telefone" class="form-control" required/>
        </div>
        <div class="form-group">
            <label for="user" class="control-label">Usuario</label>
            <input type="text" name="usuario" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="password" class="control-label">Senha</label>
            <input type="password" name="senha" class="form-control" required />
        </div>
        <button type="submit" class="btn btn-primary">
            Cadastrar
        </button>
        <button type="reset" class="btn btn-primary">
            Limpar
        </button>

    </form>

    <p><a href="index.php">Voltar</a></p>

</div>
